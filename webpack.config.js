const path = require('path');
// const LiveReloadPlugin = require('webpack-livereload-plugin');
const APP_DIR = path.resolve(__dirname, 'src');
const HtmlWebpackPlugin = require('html-webpack-plugin');


module.exports = {
    watch: false,
    mode: "development",
    devtool: "source-map",
    entry: APP_DIR + '/index.tsx',
    output: {
        filename: '[name].bundle.js',
        chunkFilename: '[name].bundle.js',
        path: path.resolve(__dirname, '.dist')

    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js','.css'],
        alias: {}
    },
    module: {
        rules: [{
            test: /\.tsx?$/,
            loader: 'ts-loader',
            options: {
                configFile: 'tsconfig.json'
            }
        }, {
            test: /\.css$/,
            loader: 'style-loader!css-loader'
        }, {
            test: /\.(png|jpg|gif|svg|eot|ttf|woff|woff2)$/,
            loader: 'url-loader',
            options: {
                limit: 10000
            }
        }],
    },
    optimization: {
        runtimeChunk: {
            name: "manifest"
        }
    },
    devServer: {
        contentBase: path.join(__dirname, '.dist'),
        compress: true,
        port: 3000
    },
    plugins: [
        // new LiveReloadPlugin({}),
        new HtmlWebpackPlugin({
            template: './src/index.html'
        })
        // new BundleAnalyzerPlugin({
        //     analyzerMode: 'static'
        // })
    ]
};