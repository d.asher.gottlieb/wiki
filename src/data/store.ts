import { createStore, combineReducers } from 'redux'

const userReducer = (store={}, action) => {
    return store
}

const pageReducer = (store={}, action) => {
    return store
}

const combined = combineReducers({
    user: userReducer,
    page: pageReducer
})


export const store = createStore(combined)