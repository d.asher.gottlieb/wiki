import { store } from './store'

export const createPage = (page): Promise<any> => {
    return Promise.resolve(store.dispatch({
        type: 'add',
        namespace: 'page',
        payload: page
    }))
}

export const removePage = () => {}

export const updatePage = (page) => {}

export const getPage = (id) => {}

export const pageReducer = (store, action) => {}