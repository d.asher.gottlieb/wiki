import * as React from 'react'
import { Nav } from 'react-bootstrap'

interface Props {
    style?: React.CSSProperties
}

export const SideNav: React.FunctionComponent<Props> = (props: Props) => {
    return <Nav style={{...props.style}} defaultActiveKey="/home" className="flex-column">
        <Nav.Link>bye</Nav.Link>
        <Nav.Link>bye</Nav.Link>
        <Nav.Link>bye</Nav.Link>
        <Nav.Link>bye</Nav.Link>
        <Nav.Link>bye</Nav.Link>
        <Nav.Link>bye</Nav.Link>
        <Nav.Link>bye</Nav.Link>
        <Nav.Link>bye</Nav.Link>
        <Nav.Link>bye</Nav.Link>
        <Nav.Link>bye</Nav.Link>
        <Nav.Link>bye</Nav.Link>
        <Nav.Link>bye</Nav.Link>
        <Nav.Link>bye</Nav.Link>
    </Nav>
}