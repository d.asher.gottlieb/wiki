import * as React from 'react'
import { Form, Button } from 'react-bootstrap'
import { useForm } from 'react-hook-form'
import ReactMde from "react-mde";
import MarkdownIt from 'markdown-it'
import { setRequired, setMinLen, renderErrMsg } from './_common/validation'
import "react-mde/lib/styles/css/react-mde-all.css";
import { createPage } from './data/page';

interface Props {}
type FormValues = {
    title: string
    content: string
    message: string
}

const mdParser = new MarkdownIt({
    html: true,
    linkify: true,
    typographer: true,
})

export const AddPage: React.FunctionComponent<Props> = (props: Props) => {
    const { register, errors, handleSubmit } = useForm<FormValues>();
    const [value, setValue] = React.useState("**Hello world!!!**");
    const [selectedTab, setSelectedTab] = React.useState<"write" | "preview">("write");

    const onSubmit = (data: FormValues) => {
        data.content = value
        createPage(data)
        .then(() => console.log('created'))
        .catch(err => console.error(err))
    }


    return <Form onSubmit={handleSubmit(onSubmit)}>
        <h3>Create New Page</h3>
        <hr style={{marginTop: 7, marginBottom: 7}}/>
        <Form.Group controlId="title">
            <Form.Label>Title</Form.Label>
            <Form.Control ref={register({required: setRequired(), minLength: setMinLen(3)})}
                type="text"
                name="title"
                placeholder="Enter Title"
                isInvalid={!!errors.title} />
            <Form.Text >
                {renderErrMsg(errors.title)}
                <div className="text-muted" hidden={!!errors.title}>
                    Tip: You can specify the full path for the new file. We will automatically create any missing directories. Learn more.
                </div>
            </Form.Text>
        </Form.Group>

        <Form.Group>
            <Form.Label>Content</Form.Label>
            <ReactMde
                classes={{textArea: 'form-control'}}
                value={value}
                onChange={setValue}
                selectedTab={selectedTab}
                onTabChange={setSelectedTab}
                generateMarkdownPreview={text => {
                    text = text || `Nothing to preview`
                    return Promise.resolve(mdParser.render(text))
                }}
            />
            <Form.Text className="text-muted">
                {renderErrMsg(errors.content)}
                To link to a (new) page, simply type [Link Title](page-slug). More examples are in the documentation.
            </Form.Text>
        </Form.Group>
        

        <Form.Group>
            <Form.Label>Commit Message</Form.Label>
            <Form.Control name="message" ref={register({required: setRequired()})} type="text" placeholder="Enter Message" />
        </Form.Group>

        <div style={{display: 'flex', justifyContent: 'flex-end'}}>
            <Button type="submit" variant="primary">Create Page</Button>
            <Button variant="secondary">Cancel</Button>
        </div>
    </Form>
} 