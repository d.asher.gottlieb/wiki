import * as React from 'react'
import { FieldError } from "react-hook-form"

export const setMinLen = (min: number) => ({
    value: min,
    message: `Length must be more than ${min}`
})

export const setMaxLen = (max: number) => ({
    value: max,
    message: `Legth must be less than ${max}`
})

export const setRequired = () => 'This is a required field'

export const renderErrMsg = (err: FieldError): JSX.Element => {
    return <div className="text-danger">
        {err && err.message}
    </div>
}
