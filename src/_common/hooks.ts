import { useEffect, useState } from 'react'
import { store } from '../data/store'

export const useUser = () => {
    const [user, setUser] = useState(
        store.getState().user
    )

    useEffect(() => {
        const unsub = store.subscribe(() => {
            const newUser = store.getState().user
            if (newUser === user) return
            setUser(newUser)
        })
        return () => unsub()
    }, ['user'])
}

// export const usePages = () => {
//     const [data, setData] = useState(
//         store.getState().pages
//     )

//     useEffect(() => {
//         const unsub = store.subscribe(() => {
//             const newData = store.getState().pages
//             if (newData === data) return
//             setData(newData)
//         })
//         return () => unsub()
//     }, ['pages'])
// }

export const usePage = (id: string) => {
    const [data, setData] = useState(
        store.getState().page
    )

    useEffect(() => {
        const unsub = store.subscribe(() => {
            const newData = store.getState().page
            if (newData === data) return
            setData(newData)
        })
        return () => unsub()
    }, [id])
}