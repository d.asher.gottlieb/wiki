export interface IPage {
    _id: any
    title: string
    content: string
    comment: string
}

export interface IPageHistory {
    version: string
    author: string
    changes: string
    lastUpdated: Date
}
