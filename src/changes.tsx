import * as React from 'react'
import { Button, ButtonGroup } from 'react-bootstrap'
import ReactDiffViewer from 'react-diff-viewer'

interface Props {}

export const ChangesPage: React.FunctionComponent<Props> = (props: Props) => {
    const [splitView, updateSplitView] = React.useState(false)
    const [showWhiteSpace, updateShowWhiteSpace] = React.useState(false)

    const getShowHideText = (): string => {
        return showWhiteSpace ? 'Hide' : 'Show'
    }

    return <div>
        <div style={{display: 'flex', justifyContent: 'flex-end'}}>
            <Button onClick={e => updateShowWhiteSpace(!showWhiteSpace)} variant='outline-secondary'>
                {getShowHideText()} whitespace changes
            </Button>
            <ButtonGroup>
                <Button variant='outline-secondary' active={!splitView}
                    onClick={e => updateSplitView(false)}>
                    Inline
                </Button>
                <Button variant='outline-secondary' active={splitView}
                    onClick={e => updateSplitView(true)}>
                    Side-by-side
                </Button>
            </ButtonGroup>
        </div>
        <ReactDiffViewer />
    </div>
}