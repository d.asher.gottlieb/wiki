import * as React from 'react'
import { Table } from 'react-bootstrap'

interface Props {}

class HistoryItem {
    version: string
    author: string
    changes: string
    lastUpdated: string
}

export const HistoryPage: React.FunctionComponent<Props> = (props: Props) => {
    const items: HistoryItem[] = []


    const renderRow = (item: HistoryItem, index: number): JSX.Element => {
        return <tr key={index}>
            <td>
                <a>{item.version}</a>
            </td>
            <td>{item.author}</td>
            <td>
                <a>{item.changes}</a>
            </td>
            <td>{item.lastUpdated}</td>
        </tr>
    }

    return <div>
        <Table hover>
            <thead>
                <tr>
                    <th>Page Version</th>
                    <th>Author</th>
                    <th>Changes</th>
                    <th>Last Updated</th>
                </tr>
            </thead>
            <tbody>
                {items.map((item, index) => renderRow(item, index))}
            </tbody>
        </Table>
    </div>
}