import * as React from 'react'
import { Button } from 'react-bootstrap'
import ReactMarkdown from 'react-markdown'

interface Props {
    content: string
}

export const ContentPage: React.FunctionComponent<Props> = (props: Props) => {
    const content = `## test`

    return <div>
        <div style={{display: "flex", justifyContent: "flex-end"}}>
            <Button variant="primary">New page</Button>
            <Button variant="outline-secondary">Page history</Button>
            <Button variant="outline-secondary">Edit</Button>
        </div>
        <hr/>
        <ReactMarkdown source={content} />
    </div>
}