import * as React from 'react'
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom";
import { AddPage } from './add'
import { ChangesPage } from './changes'
import { ContentPage } from './content'
import { HistoryPage } from './history'
import { SideNav } from './sideNav'
import { TopNav } from './topNav'

export const MainPage: React.FunctionComponent<any> = () => {
    const sideBarWidth = 150

    return <div>
        <TopNav />
        <div style={{marginTop: 56}}>
            <div style={{float: 'left'}}>
                <SideNav style={{width: sideBarWidth}} />
            </div>
            <div style={{paddingLeft: sideBarWidth}}>
                <div style={{padding: 12}}>
                    <Router>
                        <Switch>
                            <Route exact path='' component={AddPage}></Route>
                            <Route exact path='add' component={AddPage}></Route>
                        </Switch>
                    </Router>
                    {/* <AddPage /> */}
                </div>
            </div>
        </div>
    </div>
} 