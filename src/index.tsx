import 'bootstrap/dist/css/bootstrap.min.css';
import * as React from 'react'
import * as dom from 'react-dom'
import { MainPage } from './main';

dom.render(<MainPage />, document.getElementById('start'))